<?php 

    abstract class Space {

        private  $animals = [];

        public function add(Animal $a): void{
            array_push($this->animals, $a);
        }

        /**
         * 
         *
         * @param integer $index 
         * @return boolean return true if animal is removed false otherwise
         */
        public abstract function removeAnimalByIndex(int $index): bool;

        public abstract function remove(Animal $a): bool;

    }