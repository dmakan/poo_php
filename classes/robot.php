<?php
    // + public, - private, #% protected
    class Robot {

        // constructeur
        public function  __construct($nom, $pos_x, $pos_y, $direction, $taille, $couleur, $action, $autonomie, $vitesse, $aren) {

            $this->nom = $nom;
            $this->pos_x = $pos_x;
            $this->pos_y = $pos_y;
            $this->taille = ['longueur', 'largeur'];
            $this->autonomie = $autonomie;
            $this->vitesse = $vitesse;
            $this->direction = $direction;
            $this->aren = $aren;
        }

        // se deplacer
        public function get_position() {
            return [$this->pos_x, $this->pos_y, $this->direction];
        }

        // se deplacer
        public function move($longueur, $largeur) {
            $this->pos_x += $longueur;
            $this->pos_y += $largeur;
            $this->aren;
        }

        // changement de direction
        public function turn($orientation) {
            $this->direction = $orientation;
        }

        // executer son action
        public function action() {

        }

        // parler
        public function parler($mot) {

        }
    }


    $robot1 = new Robot ("Philippe", 0, 0, 45, "small", "blue", "move", "collect", 2, 2, "arena1");

    // // la position
    // echo "<pre>";
    // var_dump($robot1->get_position());

    // $robot1->turn(90);
    // $robot1->move(50, 50);

    // // nouvelle position
    // var_dump($robot1->get_position());


    $robot2 = new Robot ("Stef", 50, 50, 255, "small", "red", "run", 2, 100);
    // la position
    echo "<pre>";
    var_dump($robot1->get_position());

    $robot2->turn(90);
    $robot2->move(50, 50);

    // nouvelle position
    var_dump($robot2->get_position());
?>

<?php
    class Arena {
        private $name = "death robot arena";
        private $size_x;
        private $size_y;
        private $objects;

        public function __construct($x, $y) {
            $this->size_x = $x;
            $this->size_y = $y;
        }

        public function get_robot_pos ($bot, $pos_x, $pos_y) {
            $this->robot1 = $bot
            $this->robot1->$pos_x = $pos_x
        }
    }

    $arena1 = new Arena ("kekw", 50, 50);
?>