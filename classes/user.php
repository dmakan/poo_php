<?php
    class Users {

        public function __construct($name, $numero, $email) {
            $this->nom = $name;
            $this->num = $numero;
            $this->mail = $email;
        }

        public function get_name () {
            return $this -> nom;
        }

        public function set_name ($new) {
            $this -> nom = $new;
        }

    }

    $user1 = new Users ("Moussa", 0751374120, "makan.dianka@test.com");

    // echo $user1 -> nom . "<br />";
    // echo $user1 -> num . "<br />";
    // echo $user1 -> mail . "<br />";
    
    echo "Mon vrai nom est : ";
    echo $user1 -> get_name() . "<br />";

    echo "<br />Mon nom a été modifié par une methde <br />";
    $user1->set_name("Ali");

    echo "<br />Mon nom nouveau est : ";
    echo $user1 -> get_name() . "<br />";
?>