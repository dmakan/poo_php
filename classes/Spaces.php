<?php 

/**
 * Collection d'espaces utilisables dans le zoo !
 */
abstract class Spaces {

    // zone de déclaration
    private  $spaces;

    // zone d'initialisation
    public function  __constructor(): Spaces{
        $this->spaces = [];
    }




    // zone d'implémentation
    public abstract function add(Animal $a): void;

    public abstract function addAll(Animals $a): void;

    public abstract function new(Space $s): void;


    public function getAll(): array {
        return $this->spaces;
    }
}