<?php

    class Test implements Iterator {

        private $tableau = [];

        // constructeur 1param array
        public function __construct($tb) {
            $this->tableau = $tb;
        }

        public function rewind() {
            echo 'Retour au début du tableau<br>';
            reset($this->tableau);
        }

        public function current() {
            $tableau = current($this->tableau);
            echo 'Elément actuel : ' .$tableau. '<br>';
            return $tableau;
        }

        public function key() {
            $tableau = key($this->tableau);
            echo 'Clef : ' .$tableau. '<br>';
            return $tableau;
        }

        public function next() {
            $tableau = next($this->tableau);
            echo 'Elément suivant : ' .$tableau. '<br>';
            return $tableau;
        }

        public function valid() {
            $clef = key($this->tableau);
            $tableau = ($clef !== NULL && $clef !== FALSE);
            echo 'Valide : ';
            var_dump($tableau);
            echo '<br>';
            return $tableau;
        }
    }



    $tbtest = ['C1' => 'V1', 'C2' => 'V2', 'C3' => 'V3', 'C4' => 'V4'];
    $objet = new Test($tbtest);

    foreach ($objet as $c => $v) {
        echo $c. ' => ' .$v. '<br><br>';
    }


?> 