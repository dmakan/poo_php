<?php

    class Personnage {

        public $name = "manelle";
        public $type;
        public $taille;
        public $intel;
        public $force;
        public $genre;
        public $couleur;
        public $vitesse;
        public $apparence;
        public $attack;
        public $defense;

        public const AVATAR = "./assets/img/avatr.jpg";

        public function __construct($n, $g) {
            $this->name = $n;
            $this->genre = $g;
        }

        public function __dstruct() {
            // ici on fait des truc avant que l'objet soit déruit
        }

        // getter
        public function get_name() {
            return $this->name;
        }

        // setter
        public function set_name($nom) {                
            return $this->name = $nom;
        }
    }

class Magicien extends Personnage {

    public const AVATAR = "./assets/img/magicien/avatar.jpg";

    public function get_name() {
        return "*** ". $this -> name ." ***";
    }

    public static function get_avatar() {

        // refference à l'objet "Magicien"
        return SELF::AVATAR;

        // refference à l'objet parent "Personnage"
        // return PARENT::AVATAR;
    }
}